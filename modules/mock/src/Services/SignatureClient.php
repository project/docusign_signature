<?php

declare(strict_types=1);

namespace Drupal\docusign_signature_mock\Services;

use DocuSign\eSign\Model\EnvelopeDefinition;
use DocuSign\eSign\Model\ViewUrl;
use Drupal\Core\Url;
use Drupal\docusign_signature\Services\SignatureClient as SignatureClientOriginal;

/**
 * Decorates the client for manage signature.
 *
 * @package Drupal\docusign_signature_mock
 */
class SignatureClient extends SignatureClientOriginal {

  /**
   * {@inheritdoc}
   */
  public function createEnvelope(EnvelopeDefinition $envelopeDefinition): string {
    return 'ENVELOPE_IDENTIFIER';
  }

  /**
   * {@inheritdoc}
   */
  public function getRecipientView(string $envelopeId, $recipientViewRequest = NULL): ViewUrl {
    return new ViewUrl([
      'url' => Url::fromRoute(
        'docusign_signature_mock.signature_event_choices',
        [],
        [
          'query' => [
            'return_url' => $recipientViewRequest->getReturnUrl(),
          ],
        ]
      )->toString(TRUE)->getGeneratedUrl(),
    ]);
  }

}
