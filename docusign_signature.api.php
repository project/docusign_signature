<?php

/**
 * @file
 * Hooks provided by Docusign Signature module.
 */

declare(strict_types=1);

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Acts when DocuSign event notification call the application.
 *
 * @see Drupal\docusign_signature\Controller\CallbackEventNotification::page().
 *
 * @param string $payload
 *   The payload string.
 * @param array $context
 *   An associative array containing the request datas.
 * @param array $data
 *   THe request data.
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
function hook_docusign_event_notification_callback(string $payload, array $context, array $data): void {
  $logger = Drupal::logger('docusign_signature_examples');

  // Log DocuSign event notification callback.
  $logger->debug('Payload: @payload', [
    '@payload' => $payload,
  ]);
  $logger->debug('Request datas: @values', [
    '@values' => json_encode($data),
  ]);
}

/**
 * @} End of "addtogroup hooks".
 */
