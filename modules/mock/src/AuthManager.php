<?php

declare(strict_types=1);

namespace Drupal\docusign_signature_mock;

use Drupal\docusign_signature\AuthManager as AuthManagerOriginal;

/**
 * Decorates the DocuSign Authentication manager.
 *
 * @package Drupal\docusign_signature_mock
 */
class AuthManager extends AuthManagerOriginal {

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    return new AuthMock();
  }

}
