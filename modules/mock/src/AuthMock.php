<?php

declare(strict_types=1);

namespace Drupal\docusign_signature_mock;

use Drupal\Core\Url;
use Drupal\docusign_signature\AuthInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * DocuSign authentication mock.
 *
 * @package Drupal\docusign_signature_mock
 */
class AuthMock implements AuthInterface {

  /**
   * {@inheritdoc}
   */
  public function authCallback(string $redirectUrl = NULL): RedirectResponse {
    return $this->redirect($redirectUrl);
  }

  /**
   * {@inheritdoc}
   */
  public function checkToken(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthorizationUrl(): string {
    // Not use but required bv "AuthInterface".
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function login(): RedirectResponse {
    return $this->redirect();
  }

  /**
   * Redirect user to current page.
   *
   * @param string|null $redirectUrl
   *   The redirect URL.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   */
  private function redirect(string $redirectUrl = NULL): RedirectResponse {
    if (!isset($redirectUrl)) {
      // Redirect to current page by default.
      $redirectUrl = Url::fromRoute('<current>', [], ['absolute' => TRUE])->toString(TRUE)->getGeneratedUrl();
    }

    return new RedirectResponse($redirectUrl);
  }

}
