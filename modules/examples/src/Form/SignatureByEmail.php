<?php

declare(strict_types=1);

namespace Drupal\docusign_signature_examples\Form;

use DocuSign\eSign\Model\EnvelopeDefinition;
use DocuSign\eSign\Model\TemplateRole;
use Drupal\Core\Form\FormStateInterface;

/**
 * DocuSign signature by email example.
 *
 * @package Drupal\docusign_signature_examples\Form
 */
class SignatureByEmail extends EmbeddedSignature {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'docusign_signature_examples_by_email_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $form['carbon_copy'] = $form['signer'];
    $form['carbon_copy']['#title'] = $this->t('Carbon Copy');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEnvelopeArgs(array $values): array {
    $envelopeArgs = parent::getEnvelopeArgs($values);

    $envelopeArgs['cc_email'] = $values['carbon_copy']['email'];
    $envelopeArgs['cc_name'] = $values['carbon_copy']['name'];

    return $envelopeArgs;
  }

  /**
   * {@inheritdoc}
   */
  protected function createEnvelopeDefinition(array $envelopeArgs): EnvelopeDefinition {
    $envelopeDefinition = parent::createEnvelopeDefinition($envelopeArgs);

    // Create the template role elements to connect
    // the carbon copy to the template.
    $signer = $envelopeDefinition->getTemplateRoles()[0];

    $carbonCopy = new TemplateRole([
      'email' => $envelopeArgs['cc_email'],
      'name' => $envelopeArgs['cc_name'],
      'role_name' => 'cc',
    ]);

    // Add the TemplateRole objects to the envelope object.
    $envelopeDefinition->setTemplateRoles([$signer, $carbonCopy]);

    return $envelopeDefinition;
  }

}
