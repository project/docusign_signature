<?php

declare(strict_types=1);

namespace Drupal\docusign_signature;

use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * DocuSign authentication interface.
 *
 * @package Drupal\docusign
 */
interface AuthInterface {

  /**
   * The authorization server URL.
   *
   * @var string
   */
  const AUTHORIZATION_URL = 'https://account.docusign.com';

  /**
   * The demo authorization server URL.
   *
   * @var string
   */
  const AUTHORIZATION_DEMO_URL = 'https://account-d.docusign.com';

  /**
   * Base URI suffix.
   *
   * @var string
   */
  const BASE_URI_SUFFIX = '/restapi';

  /**
   * DocuSign login callback.
   *
   * @param string|null $redirectUrl
   *   The redirect URL.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   */
  public function authCallback(?string $redirectUrl = NULL): RedirectResponse;

  /**
   * Checker for the JWT token.
   *
   * @return bool
   *   Returns "TRUE" if token is defined and not expired.
   */
  public function checkToken(): bool;

  /**
   * Get authorization URL.
   *
   * @return string
   *   The authorization URL.
   */
  public function getAuthorizationUrl(): string;

  /**
   * DocuSign login handler.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect the user to the authorization URL.
   */
  public function login(): RedirectResponse;

}
