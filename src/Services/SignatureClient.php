<?php

declare(strict_types=1);

namespace Drupal\docusign_signature\Services;

use DocuSign\eSign\Api\TemplatesApi\GetOptions;
use DocuSign\eSign\Client\ApiException;
use DocuSign\eSign\Model\EnvelopeTemplate;
use DocuSign\eSign\Model\RecipientViewRequest;
use DocuSign\eSign\Model\ViewUrl;
use Drupal\docusign_signature\ClientBase;

/**
 * Client for manage signature.
 *
 * @package Drupal\docusign_signature\Services
 */
class SignatureClient extends ClientBase {

  /**
   * Get recipient view.
   *
   * @param string $envelopeId
   *   The envelope identifier.
   * @param \DocuSign\eSign\Model\RecipientViewRequest|null $recipientViewRequest
   *   THe recipient view request object.
   *
   * @return \DocuSign\eSign\Model\ViewUrl|null
   *   The recipient view.
   */
  public function getRecipientView(string $envelopeId, ?RecipientViewRequest $recipientViewRequest = NULL): ?ViewUrl {
    try {
      return $this->getEnvelopeApi()->createRecipientView(
        $this->tempStore->get('account_id'),
        $envelopeId,
        $recipientViewRequest
      );
    }
    catch (ApiException $e) {
      $this->logger->error('Error getting recipient view (code %code): %message', [
        '%code' => $e->getResponseBody()->errorCode,
        '%message' => $e->getMessage(),
      ]);
    }

    return NULL;
  }

  /**
   * Get recipient view request.
   *
   * @param string $authenticationMethod
   *   The authentication method to use.
   * @param array $envelopeArgs
   *   The envelope arguments.
   *
   * @return \DocuSign\eSign\Model\RecipientViewRequest
   *   The recipient view request.
   */
  public function getRecipientViewRequest(string $authenticationMethod, array $envelopeArgs): RecipientViewRequest {
    return new RecipientViewRequest([
      'authentication_method' => $authenticationMethod,
      'recipient_id' => '1',
      'return_url' => $envelopeArgs['return_url'],
      'email' => $envelopeArgs['signer_email'],
      'user_name' => $envelopeArgs['signer_name'],
    ]);
  }

  /**
   * Get the list of the Brands.
   * Retrieve all brands using the AccountBrands::List().
   *
   * @return array
   *   The brands list.
   */
  public function getBrands(): array {
    try {
      $brands = $this->getAccountsApi()->listBrands($this->tempStore->get('account_id'));
      return $brands['brands'];
    }
    catch (ApiException $e) {
      $this->logger->error('Error getting brands list (code %code): %message', [
        '%code' => $e->getResponseBody()->errorCode,
        '%message' => $e->getMessage(),
      ]);
    }

    return [];
  }

  /**
   * Get the list of the Groups.
   *
   * @return array
   *   The groups list.
   */
  public function getGroups(): array {
    // Retrieve all Groups using the GroupInformation::List.
    $accounts_api = $this->getGroupsApi();
    try {
      $groups = $accounts_api->listGroups($this->tempStore->get('account_id'));
      return $groups['groups'];
    }
    catch (ApiException $e) {
      $this->logger->error('Error getting groups list (code %code): %message', [
        '%code' => $e->getResponseBody()->errorCode,
        '%message' => $e->getMessage(),
      ]);
    }

    return [];
  }

  /**
   * Get the list of the Permission Profiles.
   *
   * @return array
   *   The permissions list.
   */
  public function getPermissionsProfiles(): array {
    try {
      $permissions = $this->getAccountsApi()->listPermissions($this->tempStore->get('account_id'));
      return $permissions['permission_profiles'];
    }
    catch (ApiException $e) {
      $this->logger->error('Error getting permissions list (code %code): %message', [
        '%code' => $e->getResponseBody()->errorCode,
        '%message' => $e->getMessage(),
      ]);
    }

    return [];
  }

  /**
   * Get template.
   *
   * @param int $templateId
   *   The template identifier.
   * @param \DocuSign\eSign\Api\TemplatesApi\GetOptions|null $options
   *   The template options.
   *
   * @return void|\DocuSign\eSign\Model\EnvelopeTemplate
   *   The DocuSign envelope template.
   */
  public function getTemplate($templateId, ?GetOptions $options = NULL): ?EnvelopeTemplate {
    try {
      return $this->getTemplatesApi()->get(
        $this->tempStore->get('account_id'),
        $templateId,
        $options
      );
    }
    catch (ApiException $e) {
      $this->logger->error('Error getting template (code %code): %message', [
        '%code' => $e->getResponseBody()->errorCode,
        '%message' => $e->getMessage(),
      ]);
    }

    return NULL;
  }

}
