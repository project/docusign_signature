<?php

declare(strict_types=1);

namespace Drupal\docusign_signature;

use Drupal\Component\Plugin\Discovery\DiscoveryInterface;

/**
 * Provides an authentication manager interface.
 *
 * @package Drupal\docusign_signature
 */
interface AuthManagerInterface extends DiscoveryInterface {

}
