<?php

declare(strict_types=1);

namespace Drupal\docusign_signature_examples\Form;

use DocuSign\eSign\Model\CompositeTemplate;
use DocuSign\eSign\Model\EnvelopeDefinition;
use DocuSign\eSign\Model\InlineTemplate;
use DocuSign\eSign\Model\Recipients;
use DocuSign\eSign\Model\ServerTemplate;
use DocuSign\eSign\Model\Signer;
use Drupal\Core\Form\FormStateInterface;

/**
 * DocuSign embedded signature with multi templates.
 *
 * @package Drupal\docusign_signature_examples\Form
 */
class EmbeddedSignatureMultiTemplates extends EmbeddedSignature {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'docusign_signature_examples_embedded_multi_templates_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $form['template_id2'] = $form['template_id'];
    $form['template_id2']['#title'] = $this->t('DocuSign template identifier 2');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function createEnvelopeDefinition(array $envelopeArgs): EnvelopeDefinition {
    $envelopeDefinition = parent::createEnvelopeDefinition($envelopeArgs);

    $envelopeDefinition->setCompositeTemplates($envelopeArgs['composite_templates']);

    // Remove useless arguments to prevent errors.
    $envelopeDefinition->setTemplateId(NULL);

    return $envelopeDefinition;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEnvelopeArgs(array $values): array {
    $envelopeArgs = parent::getEnvelopeArgs($values);

    // Manger signer by another way.
    $signer = new Signer([
      'email' => $envelopeArgs['signer_email'],
      'name' => $envelopeArgs['signer_name'],
      'role_name' => 'signer',
      'recipient_id' => '1',
      // You can set template tabs here. Declarations in envelope are ignored.
      'tabs' => [],
    ]);

    // Recipients object:
    $recipients = new Recipients([
      'signers' => [$signer],
    ]);

    $compositeTemplate1 = new CompositeTemplate([
      'server_templates' => [
        new ServerTemplate([
          'sequence' => '1',
          'template_id' => $envelopeArgs['template_id'],
        ]),
      ],
      // Add recipients in inline templates.
      'inline_templates' => [
        new InlineTemplate([
          'sequence' => '3',
          'recipients' => $recipients,
        ]),
      ],
    ]);

    $compositeTemplate2 = new CompositeTemplate([
      'server_templates' => [
        new ServerTemplate([
          'sequence' => '1',
          'template_id' => $values['template_id2'],
        ]),
      ],
      // Add recipients in inline templates.
      'inline_templates' => [
        new InlineTemplate([
          'sequence' => '3',
          'recipients' => $recipients,
        ]),
      ],
    ]);

    $envelopeArgs['composite_templates'] = [
      $compositeTemplate1,
      $compositeTemplate2,
    ];

    return $envelopeArgs;
  }

}
