<?php

declare(strict_types=1);

namespace Drupal\docusign_signature;

/**
 * Provides a DocuSign mode interface.
 *
 * @package Drupal\docusign_signature
 */
interface ModeInterface {

  /*
   * Use Docusign demonstration mode.
   *
   * @var string
   */
  const DEMO = 'demo';

  /*
   * Use Docusign production mode.
   *
   * @var string
   */
  const PRODUCTION = 'prod';

}
