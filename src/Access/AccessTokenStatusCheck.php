<?php

declare(strict_types=1);

namespace Drupal\docusign_signature\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\docusign_signature\AuthInterface;
use Drupal\docusign_signature\AuthManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Determines DocuSign access token is still valid.
 */
class AccessTokenStatusCheck implements AccessInterface, ContainerInjectionInterface {

  /**
   * The authentication service.
   *
   * @var \Drupal\docusign_signature\AuthInterface
   */
  protected AuthInterface $authService;

  /**
   * DocuSign module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Constructs a new AccessTokenStatusCheck class.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\docusign_signature\AuthManagerInterface $auth_manager
   *   The DocuSign authentication manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    AuthManagerInterface $auth_manager
  ) {
    $this->config = $config_factory->get('docusign_signature.settings');

    // Initialize authentication service.
    $this->authService = $auth_manager->createInstance(
      $this->config->get('auth_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('docusign_signature.auth_manager')
    );
  }

  /**
   * Checks access.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, Route $route): AccessResultInterface {
    $required_status = filter_var($route->getRequirement('_docusign_access_token_is_valid'), FILTER_VALIDATE_BOOLEAN);
    $access_result = AccessResult::allowedIf($required_status && $this->authService->checkToken());
    if (!$access_result->isAllowed()) {
      $access_result->setReason(
        $required_status === TRUE ?
          'This route can only be accessed with valid Docusign access token.' :
          'This route can only be accessed with invalid Docusign access token.'
      );
    }
    return $access_result;
  }

}
