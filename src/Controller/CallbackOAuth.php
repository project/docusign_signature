<?php

declare(strict_types=1);

namespace Drupal\docusign_signature\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\docusign_signature\AuthInterface;
use Drupal\docusign_signature\AuthManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Manage DocuSign OAuth callback.
 *
 * @package Drupal\docusign_signature\Controller
 */
class CallbackOAuth extends ControllerBase {

  /**
   * The authentication service.
   *
   * @var \Drupal\docusign_signature\AuthInterface
   */
  private AuthInterface $authService;

  /**
   * The page cache disabling policy.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected KillSwitch $pageCacheKillSwitch;

  /**
   * Constructs a new callback controller object.
   *
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $kill_switch
   *   The kill switch.
   * @param \Drupal\docusign_signature\AuthManagerInterface $auth_manager
   *   The DocuSign authentication manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(
    KillSwitch $kill_switch,
    AuthManagerInterface $auth_manager
  ) {
    // Initialize authentication service.
    $this->authService = $auth_manager->createInstance(
      $this->config('docusign_signature.settings')
        ->get('auth_service')
    );
    $this->pageCacheKillSwitch = $kill_switch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('page_cache_kill_switch'),
      $container->get('docusign_signature.auth_manager')
    );
  }

  /**
   * Callback page.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   */
  public function page(): RedirectResponse {
    // Disable recording of cached pages.
    $this->pageCacheKillSwitch->trigger();

    return $this->authService->authCallback();
  }

}
