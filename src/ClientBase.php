<?php

declare(strict_types=1);

namespace Drupal\docusign_signature;

use DocuSign\eSign\Api\AccountsApi;
use DocuSign\eSign\Api\EnvelopesApi;
use DocuSign\eSign\Api\GroupsApi;
use DocuSign\eSign\Api\TemplatesApi;
use DocuSign\eSign\Client\ApiClient;
use DocuSign\eSign\Client\ApiException;
use DocuSign\eSign\Configuration;
use DocuSign\eSign\Model\EnvelopeDefinition;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Client for manage signature.
 *
 * @package Drupal\docusign_signature\Services
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
abstract class ClientBase implements ClientInterface {

  /**
   * The authentication service.
   *
   * @var \Drupal\docusign_signature\AuthInterface
   */
  protected AuthInterface $authService;

  /**
   * DocuSign module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The logger for "docusign" channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Stores the DocuSign tempstore.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected PrivateTempStore $tempStore;

  /**
   * Constructs a new Client object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   * @param \Drupal\docusign_signature\AuthManagerInterface $auth_manager
   *   The DocuSign authentication manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_factory,
    PrivateTempStoreFactory $temp_store_factory,
    AuthManagerInterface $auth_manager
  ) {
    $this->config = $config_factory->get('docusign_signature.settings');
    $this->logger = $logger_factory->get('docusign');
    $this->tempStore = $temp_store_factory->get('docusign');

    // Initialize authentication service.
    $this->authService = $auth_manager->createInstance(
      $this->config->get('auth_service')
    );
  }

  /**
   * Get API client instance.
   *
   * @return \DocuSign\eSign\Client\ApiClient
   *   An API client object.
   */
  protected function getApiClient(): ApiClient {
    static $apiClient;

    if (!$apiClient) {
      // Construct API headers.
      $apiConfig = new Configuration();
      $apiConfig->setHost($this->tempStore->get('base_path'));
      $apiConfig->addDefaultHeader('Authorization', 'Bearer ' . $this->tempStore->get('access_token'));
      $apiClient = new ApiClient($apiConfig);
    }

    return $apiClient;
  }

  /**
   * Create envelope.
   *
   * @param \DocuSign\eSign\Model\EnvelopeDefinition $envelopeDefinition
   *   The envelope definition.
   *
   * @return string|null
   *   The envelope identifier.
   */
  public function createEnvelope(EnvelopeDefinition $envelopeDefinition): ?string {
    try {
      $results = $this->getEnvelopeApi()->createEnvelope($this->tempStore->get('account_id'), $envelopeDefinition);
      return $results->getEnvelopeId();
    }
    catch (ApiException $e) {
      $this->logger->error('Error during envelope creation (code %code): %message', [
        '%code' => $e->getResponseBody()->errorCode,
        '%message' => $e->getMessage(),
      ]);
    }

    return NULL;
  }

  /**
   * Getter for the Envelopes API.
   *
   * @return \DocuSign\eSign\Api\EnvelopesApi
   *   The envelope API object.
   */
  public function getEnvelopeApi(): EnvelopesApi {
    return new EnvelopesApi($this->getApiClient());
  }

  /**
   * Getter for the Templates API.
   *
   * @return \DocuSign\eSign\Api\TemplatesApi
   *   The templates API object.
   */
  public function getTemplatesApi(): TemplatesApi {
    return new TemplatesApi($this->getApiClient());
  }

  /**
   * Getter for the Accounts API.
   *
   * @return \DocuSign\eSign\Api\AccountsApi
   *   The accounts API object.
   */
  public function getAccountsApi(): AccountsApi {
    return new AccountsApi($this->getApiClient());
  }

  /**
   * Getter for the Accounts API.
   *
   * @return \DocuSign\eSign\Api\GroupsApi
   *   The group API object.
   */
  public function getGroupsApi(): GroupsApi {
    return new GroupsApi($this->getApiClient());
  }

  /**
   * Redirect user to the auth page.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response.
   */
  public function needToReAuth(): RedirectResponse {
    return $this->authService->login();
  }

}
