<?php

declare(strict_types=1);

namespace Drupal\docusign_signature_examples\Form;

use DocuSign\eSign\Model\ConnectEventData;
use DocuSign\eSign\Model\EnvelopeDefinition;
use DocuSign\eSign\Model\EnvelopeEvent;
use DocuSign\eSign\Model\EventNotification;
use DocuSign\eSign\Model\TemplateRole;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\docusign_signature\Services\SignatureClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * DocuSign embedded signature example.
 *
 * @package Drupal\f4c_microsoft_mock\Form
 */
class EmbeddedSignature extends FormBase {
  /**
   * DocuSign signature client.
   *
   * @var \Drupal\docusign_signature\Services\SignatureClient
   */
  protected SignatureClient $client;

  /**
   * Constructs a new Callback controller object.
   *
   * @param \Drupal\docusign_signature\Services\SignatureClient $signature_client
   *   The DocuSign signature client.
   */
  public function __construct(SignatureClient $signature_client) {
    $this->client = $signature_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('docusign_signature.signature_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'docusign_signature_examples_embedded_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['signer'] = [
      '#type' => 'details',
      '#title' => $this->t('Signer'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    $form['signer']['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => 'john.doe@example.com',
      ],
    ];

    $form['signer']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => $this->t('John Doe'),
      ],
    ];

    $form['template_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('DocuSign template identifier'),
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
      ],
    ];

    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 99,
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $envelopeArgs = $this->getEnvelopeArgs($form_state->getValues());

    // Create envelope.
    $envelopeId = $this->client->createEnvelope(
      $this->createEnvelopeDefinition($envelopeArgs)
    );

    if ($envelopeId) {
      // Create the Recipient View request object.
      $authenticationMethod = 'None';
      // How is this application authenticating
      // the signer? See the `authentication_method' definition
      // https://developers.docusign.com/docs/esign-rest-api/reference/envelopes/envelopeviews/createrecipient/
      $recipientViewRequest = $this->client->getRecipientViewRequest(
        $authenticationMethod,
        $envelopeArgs
      );

      // Obtain the recipient_view_url for the embedded signing.
      $results = $this->client->getRecipientView($envelopeId, $recipientViewRequest);

      if (isset($results['url'])) {
        $form_state->setResponse(
          new TrustedRedirectResponse($results['url'])
        );
      }
    }
  }

  /**
   * Creates envelope definition.
   *
   * @param array $envelopeArgs
   *   The argument to create envelope definition.
   *
   * @return \DocuSign\eSign\Model\EnvelopeDefinition
   *   The envelope definition.
   */
  protected function createEnvelopeDefinition(array $envelopeArgs): EnvelopeDefinition {
    // Define data you want to get in event notification.
    $eventData = new ConnectEventData([
      'version' => 'restv2.1',
      'format' => 'json',
      'include_data' => [
        'custom_fields',
        'recipients',
        'tabs',
      ],
    ]);

    // Create custom event notification.
    $eventNotification = new EventNotification([
      'url' => Url::fromRoute(
        'docusign_signature.callback.event_notification',
        [],
        ['absolute' => TRUE]
      )
        ->toString(TRUE)->getGeneratedUrl(),
      // When set to "true", the Document Fields associated with
      // envelope documents are included in the data.
      // Document Fields are optional custom name-value pairs added to
      // documents using the API.
      'include_document_fields' => 'false',
      // DocuSign will retry on failure if this is set.
      'require_acknowledgment' => 'true',
      // Allows you to see this in the DocuSign Admin Connect logs section.
      'logging_enabled' => 'true',
      'envelope_events' => [
        new EnvelopeEvent([
          // Valid values are:
          // Draft, Sent, Delivered, Completed, Declined, or Voided.
          'envelope_event_status_code' => 'Completed',
          // When set to "true", the PDF documents are included
          // in the message along with the updated XML.
          'include_documents' => 'false',
        ]),
        new EnvelopeEvent([
          'envelope_event_status_code' => 'Declined',
          'include_documents' => 'false',
        ]),
        new EnvelopeEvent([
          'envelope_event_status_code' => 'Sent',
          'include_documents' => 'false',
        ]),
        new EnvelopeEvent([
          'envelope_event_status_code' => 'Voided',
          'include_documents' => 'false',
        ]),
      ],
      'event_data' => $eventData,
    ]);

    // Create the envelope definition with the template identifier.
    $envelopeDefinition = new EnvelopeDefinition([
      // Valid values are:
      // * sent - The envelope is sent to the recipients.
      // * created - The envelope is saved as a draft and can be modified and sent later.
      'status' => 'sent',
      'template_id' => $envelopeArgs['template_id'],
      'event_notification' => $eventNotification,
    ]);

    // Create the template role elements to connect the signer to the template.
    $signer = new TemplateRole([
      'email' => $envelopeArgs['signer_email'],
      'name' => $envelopeArgs['signer_name'],
      'role_name' => 'signer',
    ]);

    // Add the TemplateRole objects to the envelope object.
    $envelopeDefinition->setTemplateRoles([$signer]);

    return $envelopeDefinition;
  }

  /**
   * Get envelope arguments.
   *
   * @param array $values
   *   The submitted form values.
   *
   * @return array
   *   The envelope arguments.
   */
  protected function getEnvelopeArgs(array $values): array {
    return [
      'signer_email' => $values['signer']['email'],
      'signer_name' => $values['signer']['name'],
      'template_id' => $values['template_id'],
      'return_url' => Url::fromRoute(
        'docusign_signature_examples.return',
        [],
        ['absolute' => TRUE]
      )->toString(TRUE)->getGeneratedUrl(),
    ];
  }

}
