<?php

declare(strict_types=1);

namespace Drupal\docusign_signature;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\Core\Url;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * DocuSign authentication base class.
 *
 * @package Drupal\docusign_signature\Services
 */
abstract class AuthBase implements AuthInterface {

  /**
   * DocuSign module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The logger for "docusign_signature_auth" channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Stores the DocuSign tempstore.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected PrivateTempStore $tempStore;

  /**
   * Constructs a new Auth object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger for "docusign_signature_auth" channel.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LoggerInterface $logger,
    PrivateTempStoreFactory $temp_store_factory
  ) {
    $this->config = $config_factory->get('docusign_signature.settings');
    $this->logger = $logger;
    $this->tempStore = $temp_store_factory->get('docusign');
  }

  /**
   * {@inheritdoc}
   */
  public function authCallback(?string $redirectUrl = NULL): RedirectResponse {
    if (!isset($redirectUrl)) {
      // Redirect to current page by default.
      $redirectUrl = Url::fromRoute('<current>', [], ['absolute' => TRUE])
        ->toString(TRUE)
        ->getGeneratedUrl();
    }

    // Redirect the user to the authorization URL.
    return new RedirectResponse($redirectUrl);
  }

  /**
   * {@inheritdoc}
   */
  public function checkToken(): bool {
    return (
      !empty($this->tempStore->get('access_token')) &&
      $this->tempStore->get('expiration') > \Drupal::time()->getCurrentTime()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthorizationUrl(): string {
    // In case of demo usage.
    if ($this->config->get('mode') === ModeInterface::DEMO) {
      return self::AUTHORIZATION_DEMO_URL;
    }

    return self::AUTHORIZATION_URL;
  }

}
