<?php

declare(strict_types=1);

namespace Drupal\docusign_signature_mock\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\docusign_signature\EventEnvelopeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Manage DocuSign return callback.
 *
 * @package Drupal\docusign_signature_examples\Controller
 */
class SignatureEventChoices extends ControllerBase {

  /**
   * Current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private Request $request;

  /**
   * Constructs a new callback controller object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * Return example page.
   *
   * @return array
   *   The redirect response.
   */
  public function page(): array {
    // Return URL si declared in query parameter.
    $returnUrl = Url::fromUri(
      $this->request->query->get('return_url')
    );

    $events = [
      [
        'name' => $this->t('Canceled'),
        'url' => clone $returnUrl->setOption('query', [
          'event' => EventEnvelopeInterface::CANCEL,
        ]),
      ],
      [
        'name' => $this->t('Declined'),
        'url' => clone $returnUrl->setOption('query', [
          'event' => EventEnvelopeInterface::DECLINE,
        ]),
      ],
      [
        'name' => $this->t('Signing completed'),
        'url' => clone $returnUrl->setOption('query', [
          'event' => EventEnvelopeInterface::SIGNING_COMPLETE,
        ]),
      ],
      [
        'name' => $this->t('Viewing complete'),
        'url' => clone $returnUrl->setOption('query', [
          'event' => EventEnvelopeInterface::VIEWING_COMPLETE,
        ]),
      ],
      [
        'name' => $this->t('Not managed'),
        'url' => clone $returnUrl->setOption('query', ['event' => 'not_managed']),
      ],
    ];

    return [
      '#theme' => 'signature_event_choices',
      '#events' => $events,
    ];
  }

}
