<?php

declare(strict_types=1);

namespace Drupal\docusign_signature;

/**
 * Provides a DocuSign event envelope interface.
 *
 * @package Drupal\docusign_signature
 */
interface EventEnvelopeInterface {

  /*
   * Cancel envelope event.
   * This event is triggered when user clicks on "Cancel" link.
   *
   * @var string
   */
  const CANCEL = 'cancel';

  /*
   * Decline envelope event.
   * This event is triggered when user clicks on "Refuse" link.
   *
   * @var string
   */
  const DECLINE = 'decline';

  /*
   * Signing complete envelope event.
   * This event is triggered when user signs all envelope documents.
   *
   * @var string
   */
  const SIGNING_COMPLETE = 'signing_complete';

  /*
   * Viewing complete envelope event.
   * This event is triggered when user access to documents but cannot signing.
   *
   * @var string
   */
  const VIEWING_COMPLETE = 'viewing_complete';

}
