<?php

declare(strict_types=1);

namespace Drupal\docusign_signature_examples\Form;

use DocuSign\eSign\Model\EnvelopeDefinition;
use DocuSign\eSign\Model\Tabs;
use DocuSign\eSign\Model\Text;
use Drupal\Core\Form\FormStateInterface;

/**
 * DocuSign embedded signature with prefill tabs example.
 *
 * @package Drupal\docusign_signature_examples\Form
 */
class EmbeddedSignatureWithPrefillTabs extends EmbeddedSignature {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'docusign_signature_examples_with_prefill_tabs_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $form['tabs'] = [
      '#type' => 'details',
      '#title' => $this->t('Prefill text tabs'),
      '#open' => TRUE,
      '#tree' => TRUE,
      '#attributes' => [
        'id' => 'tabs-wrapper',
      ],
    ];

    $form['tabs']['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Default DocuSign tabs (like "FullName", "Email", etc.) are locked by default. A locked tab cannot be edited in template viewer.'),
    ];

    $max = $form_state->get('num_tabs');
    if ($max === NULL) {
      $max = 1;
      $form_state->set('num_tabs', $max);
    }

    $form['tabs']['items'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];

    for ($i = 1; $i <= $max; $i++) {
      $form['tabs']['items'][] = $this->buildFieldElement($i);
    }

    $form['tabs']['add'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add tab'),
      '#submit' => ['::addTab'],
      '#ajax' => [
        'callback' => '::addTabCallback',
        'wrapper' => $form['tabs']['#attributes']['id'],
      ],
    ];

    // If there is more than one tab, add the remove button.
    if ($max > 1) {
      $form['tabs']['remove'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove tab'),
        '#submit' => ['::removeTab'],
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => '::addTabCallback',
          'wrapper' => $form['tabs']['#attributes']['id'],
        ],
      ];
    }

    return $form;
  }

  /**
   * Callback for "tabs" buttons.
   *
   * Selects and returns the fieldset with the "tabs" in it.
   *
   * @SuppressWarnings(PHPMD.UnusedFormalParameter)
   */
  public function addTabCallback(array &$form, FormStateInterface $form_state): array {
    return $form['tabs'];
  }

  /**
   * Submit handler for the "add" tabs button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addTab(array &$form, FormStateInterface $form_state): void {
    $max = $form_state->get('num_tabs');
    $form_state->set('num_tabs', $max + 1);

    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove" tabs button.
   *
   * Decrements the max counter and causes a rebuild.
   */
  public function removeTab(array &$form, FormStateInterface $form_state): void {
    $max = $form_state->get('num_tabs');
    $form_state->set('num_tabs', $max - 1);

    $form_state->setRebuild();
  }

  /**
   * Build prefill tab form element.
   *
   * @param string $label
   *   The element label.
   *
   * @return array
   *   The element render array.
   */
  protected function buildFieldElement(string $label): array {
    $element = [
      '#type' => 'details',
      '#title' => $this->t('DocuSign tab @label', [
        '@label' => $label,
      ]),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    $element['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#required' => TRUE,
    ];

    $element['value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefill value'),
      '#required' => TRUE,
    ];

    $element['locked'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Lock tab'),
      '#description' => $this->t('If checked, user cannot change prefill value.'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  protected function createEnvelopeDefinition(array $envelopeArgs): EnvelopeDefinition {
    $envelopeDefinition = parent::createEnvelopeDefinition($envelopeArgs);

    // Get the signer previously defined.
    $signer = $envelopeDefinition->getTemplateRoles()[0];

    // Prefill default tabs and custom tabs of this template.
    // Default tabs are already locked by default.
    $text_tabs = [];
    foreach ($envelopeArgs['tabs'] as &$tab) {
      $text_tabs[] = new Text([
        'tab_label' => $tab['label'],
        'value' => $tab['value'],
        'locked' => $tab['locked'] ? 'true' : 'false',
      ]);
    }

    $signer->setTabs(new Tabs([
      'text_tabs' => $text_tabs,
    ]));

    // Add the TemplateRole objects to the envelope object.
    $envelopeDefinition->setTemplateRoles([$signer]);

    return $envelopeDefinition;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEnvelopeArgs(array $values): array {
    $envelopeArgs = parent::getEnvelopeArgs($values);

    $envelopeArgs['tabs'] = $values['tabs']['items'];

    return $envelopeArgs;
  }

}
