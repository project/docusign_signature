<?php

declare(strict_types=1);

namespace Drupal\docusign_signature\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a DocuSign Authentication annotation object.
 *
 * @see \Drupal\docusign_signature\AuthManager
 * @see plugin_api
 *
 * @Annotation
 */
class DocuSignAuth extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

}
