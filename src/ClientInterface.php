<?php

declare(strict_types=1);

namespace Drupal\docusign_signature;

/**
 * Provides a client interface.
 *
 * @package Drupal\docusign_signature
 */
interface ClientInterface {

}
