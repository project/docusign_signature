<?php

declare(strict_types=1);

namespace Drupal\docusign_signature\Auth;

use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Tool\BearerAuthorizationTrait;
use Psr\Http\Message\ResponseInterface;

/**
 * Provides Docusign authentication class.
 *
 * @package Drupal\docusign_signature\Auth
 */
class DocuSign extends AbstractProvider {

  use BearerAuthorizationTrait;

  /**
   * The authorization server URL.
   *
   * @var string
   */
  public string $authorizationServer = '';

  /**
   * Allow silent authentication.
   *
   * @var bool
   */
  public bool $allowSilentAuth = TRUE;

  /**
   * Get authorization URL to begin OAuth flow.
   *
   * @return string
   *   The OAuth base authorization URL.
   *
   * @throws \LogicException
   */
  public function getBaseAuthorizationUrl(): string {
    $url = $this->getAuthServer();

    if ($this->allowSilentAuth) {
      return $url . '/oauth/auth';
    }

    return $url . '/oauth/auth?prompt=login';
  }

  /**
   * Returns the DocuSign authorization server URL.
   *
   * @return string
   *   The OAuth authorization server URL.
   *
   * @throws \LogicException
   */
  private function getAuthServer(): string {
    $url = $this->authorizationServer;
    if (!$url) {
      throw new \LogicException('AuthorizationServer URL is not set.');
    }
    return $url;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \LogicException
   */
  public function getBaseAccessTokenUrl(array $params): string {
    return $this->getAuthServer() . '/oauth/token';
  }

  /**
   * {@inheritdoc}
   *
   * @throws \LogicException
   */
  public function getResourceOwnerDetailsUrl(AccessToken $token): string {
    return $this->getAuthServer() . '/oauth/userinfo';
  }

  /**
   * Get the default scopes used by this provider.
   *
   * @return array
   *   The default scopes.
   */
  public function getDefaultScopes(): array {
    return [
      'signature',
    ];
  }

  /**
   * Check a provider response for errors.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   THe reponse to check.
   * @param array|string $data
   *   Parsed response data.
   *
   * @return bool
   *   Return "true" if response not contains error.
   *
   * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
   *   Throw exception if response contains an error.
   */
  protected function checkResponse(ResponseInterface $response, $data): bool {
    if (isset($data['error'])) {
      throw new IdentityProviderException(
        $data['error'],
        0,
        $response
      );
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function createResourceOwner(array $response, AccessToken $token): ResourceOwnerInterface {
    return new DocuSignResourceOwner($response);
  }

}
