# Docusign Signature

The Docusign Signature module allows you to electronically sign documents
using DocuSign. It sets up authentication and basic services to interface with
DocuSign.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/docusign_signature).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/docusign_signature).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

The module requires the "docusign/esign-client" composer package. See
[docusign/esign-client on Packagist](https://packagist.org/packages/docusign/esign-client)
for more details.

DocuSign uses OAuth authentication and provides JSON responses. You need
to enable these libraries to use this module.

If you download this module with Composer, this requirements
are already checked when Composer installing package.

**To request Docuusign API services, your website needs to use HTTPS.**

## Installation

To use this module, you need to download and enable the PHP libraries listed
in the requirements.

1. Install the module and its dependencies using Composer. Run the following
command:
`composer require drupal/docusign_signature`

2. Create a Docusign developer account on
[Docusign website](https://go.docusign.com/o/sandbox/).

3. Create an application and generate an API access token or a JWT Grant.
[See documentaiton](https://admindemo.docusign.com/authenticate?goTo=appsAndKeys).

4. In your APP, define the callback URI
`https://mydomain.example/docusign/signature/callback/oauth`.

## Configuration

You need to configure the DocuSign endpoints to connect with your DocuSign
application. A configuration form page is available at
**/admin/config/services/docusign/settings**.

- Choose between two authentication modes:
- Code grants
- Json Web Token (JWT): A private key is necessary.

- Enable and configure HMAC security if you need event notification.

- Switch between production and demonstration mode for easy development.
A mocked module is available if needed.

For more information:
- Module signature examples:
- Enable the "Docusign Signature Examples" module.
- Go to: **/docusign-signature/examples**
- Docusign code examples: [Docusign Code Examples](https://github.com/docusign/code-examples-php)
- Docusign developer documentation: [Docusign Developer Documentation](https://developers.docusign.com/docs/esign-rest-api/)


## Maintainers

Current maintainers:
- Sébastien Brindle - [S3b0uN3t](https://www.drupal.org/u/s3b0un3t)
