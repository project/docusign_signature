<?php

declare(strict_types=1);

namespace Drupal\docusign_signature\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\docusign_signature\Services\HmacSecurity;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Manage DocuSign event notification callback.
 *
 * @package Drupal\docusign_signature\Controller
 */
class CallbackEventNotification extends ControllerBase {

  /**
   * HMAC Security service.
   *
   * @var \Drupal\docusign_signature\Services\HmacSecurity
   */
  private HmacSecurity $hmacSecurity;

  /**
   * The page cache disabling policy.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected KillSwitch $pageCacheKillSwitch;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private Request $request;

  /**
   * Constructs a new callback controller object.
   *
   * @param \Drupal\Core\PageCache\ResponsePolicy\KillSwitch $kill_switch
   *   The kill switch.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\docusign_signature\Services\HmacSecurity $hmac_security
   *   The HMAC security service.
   */
  public function __construct(
    KillSwitch $kill_switch,
    RequestStack $request_stack,
    HmacSecurity $hmac_security
  ) {
    $this->hmacSecurity = $hmac_security;
    $this->pageCacheKillSwitch = $kill_switch;
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('page_cache_kill_switch'),
      $container->get('request_stack'),
      $container->get('docusign_signature.hmac_security')
    );
  }

  /**
   * Callback page.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function page(): Response {
    $payload = file_get_contents('php://input');

    // Disable recording of cached pages.
    $this->pageCacheKillSwitch->trigger();

    // If HMAC security was enabled.
    if (
      $this->config('docusign_signature.settings')->get('hmac.enabled') &&
      !$this->hmacSecurity->checkRequestHeaders($payload)
    ) {
      $error = 'The request is not compliant with configured HMAC Security.';

      $this->getLogger('docusign_event_notification')
        ->error($error);

      return new Response($error, 400);
    }

    $this->moduleHandler()->invokeAll('docusign_event_notification_callback', [
      $payload,
      $this->request->query->all(),
    ]);

    return new Response(NULL, 204);
  }

}
