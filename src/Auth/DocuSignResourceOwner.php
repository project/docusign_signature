<?php

declare(strict_types=1);

namespace Drupal\docusign_signature\Auth;

use League\OAuth2\Client\Provider\ResourceOwnerInterface;

/**
 * Represent the resource owner authenticated with a service provider.
 *
 * @package Drupal\docusign
 */
class DocuSignResourceOwner implements ResourceOwnerInterface {

  /**
   * Raw response.
   *
   * @var array
   */
  protected array $response;

  /**
   * The default or selected account.
   * If targetAccountId option was set then that account will be selected.
   * Else (usual case), the user's default account will be selected.
   *
   * Example:
   *      "account_id": "7f09961a-a22e-4ea2-8395-aaaaaaaaaaaa",
   *      "is_default": true,
   *      "account_name": "ACME Supplies",
   *      "base_uri": "https://demo.docusign.net",
   *      "organization": {
   *          "organization_id": "9dd9d6cd-7ad1-461a-a432-aaaaaaaaaaaa",
   *          "links": [
   *              {
   *                  "rel": "self",
   *                  "href":
   *   "https://account-d.docusign.com/organizations/9dd9d6cd-7ad1-461a-a432-aaaaaaaaaaaa"
   *              }
   *          ]
   *      }
   *
   * @var array[account_idis_defaultaccount_namebase_url
   *      (optional) <organization> info ]
   */
  protected array $accountInfo = [];

  /**
   * Creates new resource owner.
   *
   * @param array $response
   *   The resource owner response.
   *
   * @throws \Exception
   *   If an account is selected but not found.
   */
  public function __construct(array $response = []) {
    $this->response = $response;

    // Find the default account info.
    foreach ($response['accounts'] as &$accountInfo) {
      if ($accountInfo['is_default']) {
        $this->accountInfo = $accountInfo;
        break;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): ?string {
    return $this->getUserId();
  }

  /**
   * Get resource owner identifier.
   *
   * @return string|null
   *   The resource owner identifier.
   */
  public function getUserId(): ?string {
    return $this->response['sub'] ?: NULL;
  }

  /**
   * Get resource owner email.
   *
   * @return string|null
   *   The resource owner email.
   */
  public function getEmail(): ?string {
    return $this->response['email'] ?: NULL;
  }

  /**
   * Get resource owner name.
   *
   * @return string|null
   *   The resource owner name.
   */
  public function getName(): ?string {
    return $this->response['name'] ?: NULL;
  }

  /**
   * Get selected account info.
   *
   * @return array
   *   The selected account info. The expected structure is:
   *   [account_id, is_default, account_name, base_url]
   */
  public function getAccountInfo(): array {
    return $this->accountInfo;
  }

  /**
   * Get array of account info for the user's accounts.
   * An account's info may include organization info.
   *
   * @return array
   *   The resource owner accounts.
   */
  public function getAccounts(): array {
    return $this->response['accounts'];
  }

  /**
   * {@inheritdoc}
   */
  public function toArray(): array {
    return $this->response;
  }

}
