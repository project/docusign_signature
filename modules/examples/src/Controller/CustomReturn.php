<?php

declare(strict_types=1);

namespace Drupal\docusign_signature_examples\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\docusign_signature\EventEnvelopeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Manage DocuSign return callback.
 *
 * @package Drupal\docusign_signature_examples\Controller
 */
class CustomReturn extends ControllerBase {

  /**
   * Current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private Request $request;

  /**
   * Constructs a new callback controller object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * Return example page.
   *
   * @return array
   *   The redirect response.
   */
  public function page(): array {
    // Query parameter returned by DocuSign.
    $event = $this->request->query->get('event');

    switch ($event) {
      case EventEnvelopeInterface::CANCEL:
        $value = $this->t('Canceled');
        break;

      case EventEnvelopeInterface::DECLINE:
        $value = $this->t('Declined');
        break;

      case EventEnvelopeInterface::SIGNING_COMPLETE:
        $value = $this->t('Signing completed');
        break;

      case EventEnvelopeInterface::VIEWING_COMPLETE:
        $value = $this->t('Viewing complete');
        break;

      default:
        $value = $this->t('Not managed');
        break;
    }

    return [
      '#markup' => $this->t(
        'Event: @event. This event parameter is supplied by DocuSign.
 Since it could have been spoofed, don\'t make business decisions based on
 its value. Instead, query DocuSign as appropriate.',
        [
          '@event' => $value,
        ]
      ),
    ];
  }

}
