<?php

declare(strict_types=1);

namespace Drupal\docusign_signature\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\docusign_signature\AuthManagerInterface;
use Drupal\docusign_signature\ModeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the docusign signature configuration form.
 *
 * @internal
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The available authentication modes.
   *
   * @var array
   */
  private array $authPluginsAvailable;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\docusign_signature\AuthManagerInterface $auth_manager
   *   The DocuSign authentication manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AuthManagerInterface $auth_manager) {
    parent::__construct($config_factory);

    $this->authPluginsAvailable = $auth_manager->getDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('docusign_signature.auth_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'docusign_signature_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['docusign_signature.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('docusign_signature.settings');

    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Integration Key'),
      '#default_value' => $config->get('client_id'),
      '#required' => TRUE,
    ];

    $form['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Integration Key Secret'),
      '#default_value' => $config->get('client_secret'),
      '#required' => TRUE,
    ];

    $options = [];
    foreach ($this->authPluginsAvailable as &$plugin) {
      $options[$plugin['id']] = $plugin['label'];
    }

    $form['auth_service'] = [
      '#type' => 'radios',
      '#title' => $this->t('Authentication service'),
      '#options' => $options,
      '#default_value' => $config->get('auth_service'),
      '#required' => TRUE,
    ];

    $form['allow_silent_authentication'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow silent authentication'),
      '#description' => $this->t('A user can be silently authenticated if they have an active login session on another tab of the same browser.'),
      '#default_value' => $config->get('allow_silent_authentication'),
    ];

    $form['mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Mode'),
      '#options' => [
        ModeInterface::DEMO => $this->t('Demo'),
        ModeInterface::PRODUCTION => $this->t('Production'),
      ],
      '#default_value' => $config->get('mode'),
      '#required' => TRUE,
    ];

    $form['private_key_file'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private key file'),
      '#default_value' => $config->get('private_key_file'),
      '#states' => [
        'visible' => [
          ':input[name="auth_service"]' => ['value' => 'jwt'],
        ],
        'required' => [
          ':input[name="auth_service"]' => ['value' => 'jwt'],
        ],
      ],
    ];

    $form['impersonated_user_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User identifier'),
      '#default_value' => $config->get('impersonated_user_id'),
      '#states' => [
        'visible' => [
          ':input[name="auth_service"]' => ['value' => 'jwt'],
        ],
        'required' => [
          ':input[name="auth_service"]' => ['value' => 'jwt'],
        ],
      ],
    ];

    $form['hmac'] = [
      '#type' => 'details',
      '#title' => $this->t('HMAC Security'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    $form['hmac']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable HMAC protection (recommended)'),
      '#description' => $this->t('You can enabled HMAC security for DocuSign Connect (<a href="@url">More informations</a>).', [
        '@url' => 'https://developers.docusign.com/platform/webhooks/connect/hmac/',
      ]),
      '#default_value' => $config->get('hmac.enabled'),
    ];

    $form['hmac']['secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret key'),
      '#default_value' => $config->get('hmac.secret'),
      '#states' => [
        'visible' => [
          ':input[name="hmac[enabled]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="hmac[enabled]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $private_key_file = $form_state->getValue('private_key_file');

    if (
      !empty($private_key_file) &&
      !file_exists($private_key_file)
    ) {
      $form_state->setErrorByName('private_key_file', $this->t("The specified file doesn't exist."));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('docusign_signature.settings')
      ->set('allow_silent_authentication', (bool) $form_state->getValue('allow_silent_authentication'))
      ->set('auth_service', $form_state->getValue('auth_service'))
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('hmac.enabled', $form_state->getValue(['hmac', 'enabled']))
      ->set('hmac.secret', $form_state->getValue(['hmac', 'secret']))
      ->set('impersonated_user_id', $form_state->getValue('impersonated_user_id'))
      ->set('mode', $form_state->getValue('mode'))
      ->set('private_key_file', $form_state->getValue('private_key_file'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
